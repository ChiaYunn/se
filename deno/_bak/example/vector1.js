import * as V from '../../vector.js'

let a = [2,4,6]
let b = [2,2,2]

console.log('add(a,b)=', V.add(a,b))
console.log('sub(a,b)=', V.sub(a,b))
console.log('mul(a,b)=', V.mul(a,b))
console.log('div(a,b)=', V.div(a,b))
console.log('dot(a,b)=', V.dot(a,b))
