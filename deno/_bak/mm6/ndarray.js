const T = require('./tensor')
const ND = module.exports = {}

ND.toTensor = function (o) {
  return T.ndarray2tensor(o)
}
