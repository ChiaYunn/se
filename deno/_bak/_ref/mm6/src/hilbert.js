const E = module.exports = {}
const V = require('./vector')
const C = require('./calculus')
const F = require('./function')

// 函數的 Hilbert 空間向量長度
E.hilbertLength = function (f, a, b) {
  let ff = function (f) { 
    return function (x) {
      let fx = f(x)
      return fx*fx
    }
  }
  return Math.sqrt(C.integral(ff, a, b))
}

// 兩函數的距離
E.hilbertDistance = function (fx, fy, a, b) {
  let fd = F.sub(fx, fy)
  return H.hilbertLength(fd, a, b)
}

