const expect = require('se6').expect
const mm6 = require('../src')
const {oo} = mm6

describe('mm6 test', function() {
  it('Complex test', function() {
    let a = oo('1+2i')
    let b = oo('2+1i')
    expect(a.add(b).sub(b)).to.equal(a)
    let e = a.mul(b).div(b) // .toFixed(4)
    console.log('e=%j', e)
    expect(e.v.r).to.near(a.v.r)
  })
})

