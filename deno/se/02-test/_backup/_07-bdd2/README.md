### 練習 -- BDD2

講解： 從 lodash 中再度挑選第三個函數，但是這次採用 BDD 的方式。

BDD : Behavior Driven Development  (行為驅動開發，測試函數的寫法比較像口語)

1. 挑選第三個 lodash 函數。 (上次已完成)
2. 選定後用 mocha + chai 寫測試函數，寫好後直接打 mocha，此時會報錯誤。(上次已完成)
3. 接著再開始寫該函數的實作程式，直到 mocha 沒有報錯為止。 (這次要做這個動作)

範例：

```
PS D:\ccc\ccc109a\se\deno\se\02-test\07-bdd2\test> deno test concat_test.ts
Check file:///D:/ccc/ccc109a/se/deno/se/02-test/07-bdd2/test/.deno.test.ts
running 1 tests
test concat ... ok (13ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (161ms)

PS D:\ccc\ccc109a\se\deno\se\02-test\07-bdd2\test> deno test .
Check file:///D:/ccc/ccc109a/se/deno/se/02-test/07-bdd2/test/.deno.test.ts
running 3 tests
test chunk ... ok (33ms)
test compact ... ok (14ms)
test concat ... ok (5ms)

test result: ok. 3 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (484ms)
```

