# 軟體工程

* [用十分鐘搞懂 《系統分析、軟體工程、專案管理與設計模式》](https://www.slideshare.net/ccckmit/ss-57260495)
* [十分鐘系列:用JavaScript 實踐《軟體工程》的那些事兒！](https://www.slideshare.net/ccckmit/javascript-73015924)
* [軟體工程(總結篇)](https://www.slideshare.net/ccckmit/ss-85236849)
* [用十分鐘《讓你的專案一開始就搞砸》！](https://www.slideshare.net/ccckmit/ss-67392673)
* [假如我是一個 PM？ (軟體專案管理的那些事兒)](https://www.slideshare.net/ccckmit/pm-81399724)

## deno

* [Package Manager like npm for Deno - Welcome to Trex](https://morioh.com/p/5e1454f2c1e6)

